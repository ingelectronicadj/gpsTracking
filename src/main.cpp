#include <Arduino.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>

#define TX 4
#define RX 3

SoftwareSerial serialgps(TX,RX);
TinyGPS gps; //Declaramos el objeto gps
//Declaramos la variables para la obtención de datos
int year;
byte month, day, hour, minute, second;
unsigned long chars;
unsigned short sentences, failed_checksum;

char dato=' '; //protocolo NMEA (National Marine electronics Asociation)

void initSerial(){
  Serial.begin(9600);
  Serial.println(" Inicializando comunicacion Serial ");
}

void initGPS(){
  serialgps.begin(9600);
  Serial.println(" ---GPS buscando senal satelital--- ");
}

void setup(){
  initSerial();
  initGPS();
}

void loop(){
  // if(serialgps.available()){
  //   dato = serialgps.read();
  //   Serial.print(dato);
  //   delay (50);
  // }
  
  while(serialgps.available()){
    dato = serialgps.read();
    if(gps.encode(dato)) {
      float lat, lon;
      gps.f_get_position(&lat, &lon);
      Serial.println("Latitud/Longitud: " + String(lat,5) + ", " + String(lon,5));
   
      gps.crack_datetime(&year,&month,&day,&hour,&minute,&second);
      Serial.println("Fecha: " + String(day, DEC) + "/" + String(month, DEC) + "/" + String(year));
      Serial.println(" Hora: " + String(hour, DEC) + ":" + String(minute, DEC) + ":" + String(second, DEC));
      Serial.println("Altitud: " + String(gps.f_altitude()) +  " mts");
      Serial.println("Azimut: " + String(gps.f_course()) + "º");
      Serial.println("Velocidad: " + String(gps.f_speed_kmph() )+ "(kmph)");
      Serial.println("Satelites: " + String(gps.satellites()));
      Serial.println();
      gps.stats(&chars, &sentences, &failed_checksum);
      Serial.println(String(chars) + "/-/" + String(sentences) + "/-/" + String(failed_checksum));
    }
  }
}
